# -*- coding: utf-8 -*-


###################################
# Partie données parc et jardins ##
###################################
import urllib.request
import json

with urllib.request.urlopen('https://download.data.grandlyon.com/ws/grandlyon/com_donnees_communales.comparcjardin_1_0_0/all.json?maxfeatures=364&start=1') as data_json:
    parcs = json.load(data_json)


 # ici on harmonise ! 
 
for item in parcs["values"]:
 	# on enlève quelques colonnes qui ne nous intéressent pas :
    del item['photo'], item['ann_ouvert'], item['uid'], item['reglement'], item['gestion']
    del item['esp_can'], item['id_ariane'],item['openinghoursspecification']
    del item['precision_horaires'],item['gid'],item['num'],item['codepost'] 
    for cle,valeur in item.items():
        if valeur == 'nr':
            item[cle] = ''
        elif isinstance(valeur,str):
            item[cle] = valeur.title()
 	
    if item['chien'] not in ('Oui', 'Non', 'Tenus En Laisse'): #sur 2 lignes, le 'Non' était dit autrement
        item['chien'] = 'Non'

    item['Commune'] = item.pop('commune')
    item['NomParc'] = item.pop('nom')
    item['NoVoie'] = item.pop('numvoie')
    item['Voie'] = item.pop('voie')
    item['CodeInsee'] = item.pop('code_insee')
    item['Clos'] = item.pop('clos')
    item['Surface'] = item.pop('surf_tot_m2')
    item['Label'] = item.pop('label')
    item['Chien'] = item.pop('chien')
    item['Toilettes'] = item.pop('toilettes')
    item['Acces'] = item.pop('acces')
    item['Circulation'] = item.pop('circulation')
    item['PointEau'] = item.pop('eau')
    item['TypeEquipement'] = item.pop('type_equip')
    #print(item, "\n\n")
	
#########################################
## Partie données equipements sportifs ##
#########################################

import csv


lecteur = csv.DictReader(open('Data_sport_2016.csv','r',newline='', encoding='latin-1'), delimiter=';')
sport_long = [dict(ligne) for ligne in lecteur]
sport=[]
for ligne in sport_long:
	ligne1 = dict()
	for cle,valeur in ligne.items():
		if cle in {'ComInseeArr','ComLib','ActCode','InsNumeroInstall','EquipementId','EquipementTypeLib','EquSurfaceEvolution','EquGpsX','EquGpsY','ActLib','InsNom','NatureLibelle','NatureSolLib','InsNoVoie','InsLibelleVoie'}:
			ligne1[cle] = valeur
	#Maintenant on renomme certains attributs pour qu'ils aient le même nom dans chaque table :
	ligne1['CodeInsee'] = ligne1.pop('ComInseeArr')
	ligne1['Commune'] = ligne1.pop('ComLib')
	ligne1['NoVoie']  = ligne1.pop('InsNoVoie')
	ligne1['Voie'] = ligne1.pop('InsLibelleVoie')
	ligne1['ActiviteCode'] = ligne1.pop('ActCode')
	ligne1['NoInstallation'] = ligne1.pop('InsNumeroInstall')
	ligne1['EquId'] = ligne1.pop('EquipementId')
	ligne1['EquType'] = ligne1.pop('EquipementTypeLib')
	ligne1['EquSurface'] = ligne1.pop('EquSurfaceEvolution')
	ligne1['ActiviteDescript'] = ligne1.pop('ActLib')
	ligne1['NomInstallation'] = ligne1.pop('InsNom')
	ligne1['Nature'] = ligne1.pop('NatureLibelle')
	ligne1['NatureSol'] = ligne1.pop('NatureSolLib')
	

	sport.append(ligne1)
	#print(ligne1,"\n")
del sport_long



##############################################
## PARTIE INSERTION DES DONNEES DANS LA Bad BDD ##
##############################################

# on importe le module permettant d'interagir avec la base de données
import sqlite3 as sq

# On se connecte ? la base de données
connexion = sq.connect('Base2.db')

# On crée un curseur pour pouvoir entrer des requêtes SQL
monCurseur = connexion.cursor()
clesInstallation, clesParcs = [],[]
## on crée les requetes
for ligne in sport:
    if (ligne['NoInstallation'],ligne['ActiviteCode']) not in clesInstallation:
        requete = (ligne['CodeInsee'], ligne['Commune'], ligne['NoInstallation'],
                   ligne['NomInstallation'], ligne['Voie'], ligne['NoVoie'],
                   ligne['EquId'], ligne['EquType'], ligne['EquSurface'], 
                   ligne['EquGpsX'], ligne['EquGpsY'], ligne['Nature'], 
                   ligne['NatureSol'], ligne['ActiviteCode'], ligne['ActiviteDescript']
                   )
        monCurseur.execute('INSERT INTO SPORT VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', requete)
        clesInstallation.append((ligne['NoInstallation'],ligne['ActiviteCode']))


for ligne in parcs["values"]:
    if (ligne['NomParc']) not in clesParcs:
        requete = (ligne['CodeInsee'], ligne['Commune'], ligne['NomParc'], ligne['NoVoie'],ligne['Voie'],ligne['Surface'],ligne['Label'],ligne['Clos'],ligne['Acces'],ligne['Circulation'], ligne['TypeEquipement'],ligne['Toilettes'],ligne['PointEau'],ligne['Chien'])
        monCurseur.execute('INSERT INTO PARC VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', requete)
        clesParcs.append((ligne['NomParc']))
## on sauvegarde les modifications
connexion.commit()


