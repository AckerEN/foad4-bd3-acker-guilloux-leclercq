
DROP TABLE IF EXISTS PARC;
DROP TABLE IF EXISTS SPORT;



CREATE TABLE PARC (
  CodeInsee INTEGER,
  Commune TEXT,
  NomParc TEXT,
  NoVoie INTEGER,
  Voie TEXT,
  Surface REAL,
  Label TEXT,
  Clos TEXT,
  Acces TEXT,
  Circulation TEXT,
  TypeEquipement TEXT,
  Toilettes TEXT,
  PointEau TEXT,
  Chien TEXT,
  PRIMARY KEY (NomParc)
);


CREATE TABLE SPORT (
  CodeInsee INTEGER,
  Commune TEXT,
  NoInstallation INTEGER,
  NomInstallation TEXT,
  Voie TEXT,
  NoVoie INTEGER,
  EquId INTEGER,
  EquType TEXT,
  EquSurface TEXT,
  EquGpsx TEXT,
  EquGpsy TEXT,
  Nature TEXT,
  NatureSol TEXT,
  ActiviteCode TEXT,
  ActiviteDescript TEXT,
  PRIMARY KEY (NoInstallation,ActiviteCode)
);
