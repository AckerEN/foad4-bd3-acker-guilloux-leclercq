# -*- coding: utf-8 -*-


###################################
# Partie données parc et jardins ##
###################################

## on importe le module urlib pour se connecter au site et récuperer les données
## les données étant au format JSON, on import le module json pour pouvoir traiter les données
import urllib.request
import json

with urllib.request.urlopen('https://download.data.grandlyon.com/ws/grandlyon/com_donnees_communales.comparcjardin_1_0_0/all.json?maxfeatures=364&start=1') as data_json:
    parcs = json.load(data_json)


 # ici on harmonise les noms d'attributs 
 
for item in parcs["values"]:
 	# on enlève quelques colonnes qui ne nous intéressent pas :
    del item['photo'], item['ann_ouvert'], item['uid'], item['reglement'], item['gestion']
    del item['esp_can'], item['id_ariane'],item['openinghoursspecification']
    del item['precision_horaires'],item['gid'],item['num'],item['codepost'] 
    for cle,valeur in item.items():
        if valeur == 'nr':
            item[cle] = ''
        elif isinstance(valeur,str):
            item[cle] = valeur.title()
 	
    if item['chien'] not in ('Oui', 'Non', 'Tenus En Laisse'): #sur 2 lignes, le 'Non' était dit autrement
        item['chien'] = 'Non'

    item['Commune'] = item.pop('commune')
    item['NomParc'] = item.pop('nom')
    item['NoVoie'] = item.pop('numvoie')
    item['Voie'] = item.pop('voie')
    item['CodeInsee'] = item.pop('code_insee')
    item['Clos'] = item.pop('clos')
    item['Surface'] = item.pop('surf_tot_m2')
    item['Label'] = item.pop('label')
    item['Chien'] = item.pop('chien')
    item['Toilettes'] = item.pop('toilettes')
    item['Acces'] = item.pop('acces')
    item['Circulation'] = item.pop('circulation')
    item['PointEau'] = item.pop('eau')
    item['TypeEquipement'] = item.pop('type_equip')
    
	
#########################################
## Partie données equipements sportifs ##
#########################################

# on importe le module csv pour pouvoir traiter les données du fichier .csv
import csv


lecteur = csv.DictReader(open('Data_sport_2016.csv','r',newline='', encoding='latin-1'), delimiter=';')
equipements_long = [dict(ligne) for ligne in lecteur]
equipements=[]
for ligne in equipements_long:
	ligne1 = dict()
	for cle,valeur in ligne.items():
		if cle in {'ComInseeArr','ComLib','ActCode','InsNumeroInstall','EquipementId','EquipementTypeLib','EquSurfaceEvolution','EquGpsX','EquGpsY','ActLib','InsNom','NatureLibelle','NatureSolLib','InsNoVoie','InsLibelleVoie'}:
			ligne1[cle] = valeur
	#Maintenant on renomme certains attributs pour qu'ils aient le même nom dans chaque table :
	ligne1['CodeInsee'] = ligne1.pop('ComInseeArr')
	ligne1['Commune'] = ligne1.pop('ComLib')
	ligne1['NoVoie']  = ligne1.pop('InsNoVoie')
	ligne1['Voie'] = ligne1.pop('InsLibelleVoie')
	ligne1['ActiviteCode'] = ligne1.pop('ActCode')
	ligne1['NoInstallation'] = ligne1.pop('InsNumeroInstall')
	ligne1['EquId'] = ligne1.pop('EquipementId')
	ligne1['EquType'] = ligne1.pop('EquipementTypeLib')
	ligne1['EquSurface'] = ligne1.pop('EquSurfaceEvolution')
	ligne1['ActiviteDescript'] = ligne1.pop('ActLib')
	ligne1['NomInstallation'] = ligne1.pop('InsNom')
	ligne1['Nature'] = ligne1.pop('NatureLibelle')
	ligne1['NatureSol'] = ligne1.pop('NatureSolLib')
	

	equipements.append(ligne1)
	
del equipements_long





##############################################
## PARTIE INSERTION DES DONNEES DANS LA BDD ##
##############################################

# on importe le module permettant d'interagir avec la base de données
import sqlite3 as sq

# On se connecte ? la base de données
connexion = sq.connect('Base3.db')

# On crée un curseur pour pouvoir entrer des requêtes SQL
monCurseur = connexion.cursor()
clesInstallation, clesEquipement, clesCommune, clesActivité, clesParcs = [],[],[],[],[]
## on crée les requetes
for ligne in equipements:
    if (ligne['CodeInsee'], ligne['NoInstallation']) not in clesInstallation:
        requete = (ligne['CodeInsee'], ligne['NoInstallation'], ligne['NomInstallation'],ligne['Voie'],ligne['NoVoie'])
        monCurseur.execute('INSERT INTO INSTALLATION(CodeInsee,NoInstallation,NomInstallation,Voie,Novoie) VALUES (?,?,?,?,?)', requete)
        clesInstallation.append((ligne['CodeInsee'], ligne['NoInstallation']))
    if (ligne['CodeInsee'], ligne['NoInstallation'],ligne['EquId']) not in clesEquipement:
        requete = (ligne['CodeInsee'],ligne['NoInstallation'],ligne['EquId'],ligne['EquType'],ligne['EquSurface'],ligne['EquGpsX'],ligne['EquGpsY'],ligne['Nature'],ligne['NatureSol'])
        monCurseur.execute('INSERT INTO EQUIPEMENT VALUES (?,?,?,?,?,?,?,?,?)', requete)
        clesEquipement.append((ligne['CodeInsee'], ligne['NoInstallation'],ligne['EquId']))
    if ligne['CodeInsee'] not in clesCommune:
        requete = (ligne['CodeInsee'],ligne['Commune'])
        monCurseur.execute('INSERT INTO COMMUNE VALUES (?,?)', requete)
        clesCommune.append(ligne['CodeInsee'])
    
    ####################################################################################
    ## TODO : ecrire le bloc d'instructions qui permet de completer la table Activité ##
    ####################################################################################
    

for ligne in parcs["values"]:
    if (ligne['CodeInsee'], ligne['NomParc']) not in clesParcs:
        requete = (ligne['CodeInsee'], ligne['NomParc'], ligne['NoVoie'],ligne['Voie'],ligne['Surface'],ligne['Label'],ligne['Clos'],ligne['Acces'],ligne['Circulation'], ligne['TypeEquipement'],ligne['Toilettes'],ligne['PointEau'],ligne['Chien'])
        monCurseur.execute('INSERT INTO PARC VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', requete)
        clesParcs.append((ligne['CodeInsee'], ligne['NomParc']))

## on sauvegarde les modifications
connexion.commit()
## on ferme la connexion à la base de donnée
connexion.close()

################################
### PARTIE ELEVE A COMPLETER ###
################################

def executeRequeteSQL(requete, bdd):
    ''' prend en argument une requete SQL de type de str
    et l'execute sur la base de donnée bdd de type str
    On supposera la base de donnée non ouverte, et on fermera la connexion
    après avoir enregistré les changements'''

    try:
        ## TODO ##
    except sq.OperationalError as erreur:
        print(erreur)
    except sq.IntegrityError as erreur:
        print(erreur)