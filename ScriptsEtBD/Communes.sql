DROP TABLE IF EXISTS COMMUNE;
DROP TABLE IF EXISTS PARC;
DROP TABLE IF EXISTS INSTALLATION;
DROP TABLE IF EXISTS EQUIPEMENT;
DROP TABLE IF EXISTS ACTIVITÉ;


CREATE TABLE COMMUNE (
  CodeInsee TEXT,
  Commune TEXT,
  CodePostal TEXT,
  PRIMARY KEY (CodeInsee)
);

CREATE TABLE INSTALLATION (
  CodeInsee INTEGER,
  NoInstallation INTEGER,
  NomInstallation TEXT,
  Voie TEXT,
  NoVoie INTEGER,
  PRIMARY KEY (CodeInsee, noinstallation),
  FOREIGN KEY (CodeInsee) REFERENCES COMMUNE(CodeInsee)
);

CREATE TABLE PARC (
  CodeInsee INTEGER,
  NomParc TEXT,
  NoVoie INTEGER,
  Voie TEXT,
  Surface REAL,
  Label TEXT,
  Clos TEXT,
  Acces TEXT,
  Circulation TEXT,
  TypeEquipement TEXT,
  Toilettes TEXT,
  PointEau TEXT,
  Chien TEXT,
  PRIMARY KEY (CodeInsee, NomParc),
  FOREIGN KEY (CodeInsee) REFERENCES COMMUNE (CodeInsee)
);


CREATE TABLE EQUIPEMENT (
  CodeInsee INTEGER,
  NoInstallation INTEGER,
  EquId INTEGER,
  EquType TEXT,
  EquSurface TEXT,
  EquGpsx TEXT,
  EquGpsy TEXT,
  Nature TEXT,
  NatureSol TEXT,
  PRIMARY KEY (CodeInsee, NoInstallation, EquId),
  FOREIGN KEY (CodeInsee, NoInstallation) REFERENCES INSTALLATION (CodeInsee, NoInstallation)
);

CREATE TABLE ACTIVITÉ (
  CodeInsee INTEGER,
  NoInstallation INTEGER,
  EquId INTEGER,
  ActiviteCode TEXT,
  ActiviteDescript TEXT,
  PRIMARY KEY (CodeInsee, NoInstallation, EquId, activitecode),
  FOREIGN KEY (CodeInsee, NoInstallation, EquId) REFERENCES EQUIPEMENT(CodeInsee, NoInstallation, EquId)
);
