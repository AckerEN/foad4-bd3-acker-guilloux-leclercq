# Rendu pour le Bloc 4 du DIU EIL - juin 2020

**Antoine ACKER, Yves GUILLOUX, Vincent LECLERCQ**

Ce dépot contient notre rendu pour le bloc 4. Il est composé comme suis :

* un document de présentation 
* un document élèves
* un dossier contenant les scripts SQL, pyhton et les bases de données
* un dossier pour les images du documents élèves

