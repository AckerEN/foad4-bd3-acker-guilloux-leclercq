# Diu EIL - Bloc 4 - FOAD
## Base de données - BD3
*Antoine Acker, Yves Guilloux, Vincent Leclercq*

Conception d'une base de données à partir des données Parcs et installations sportives de le plateforme OpenData de la métropole de Lyon

### Description de l'activité:

L'objectif est de concevoir une base de données à partir de deux jeux de données OpenData de la métropole de Lyon. Elle se présente sous la forme d'un TD/TP pour un total de 4h élève. 

*Prérequis :*

Le TD se place dans la partie "bases de données" du programme de terminale. On suppose évidemment une connaissance de python ainsi que la partie "traitement des données en table" de première. On suppose également qu'une première séquence sur les bases de données à été faite, présentant SQL et la notion de modèle relationnel, de table, d'attribut, de clé primaire, de clé étrangère...

*Points travaillés :*

Cette séquence travaille plus particulièrement l'importance de la conception de base de données. On souhaite permettre aux élèves de comprendre l'interêt d'une base de données par rapport à une simple table, ainsi que les anomalies qui peuvent résulter d'une mauvaise normalisation.
On travaillera aussi le formatage/nettoyage des données pour pouvoir les utiliser, montrant comment cela est possible avec python.