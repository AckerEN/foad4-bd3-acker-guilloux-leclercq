# Parc et installations sportives de la métropole de Lyon - Base de données, requêtes SQL et anomalies.
## Construction d'une base de données à partir de jeux de données de la métropole

**Introduction**

La métropole de Lyon, comme de nombreuses collectivités et administrations, fournit des jeux de données ouvertes, c'est-à-dire utilisables par tout un chacun. Cela fait partie d'une politique dite d'opendata.
Nous allons chercher à récupérer et utiliser les données mises à disposition par la métropole de Lyon sur ses parcs et jardins d'une part, et sur ses installations sportives d'autre part, afin de les consolider en une seule base de données.
Cette base de données devrait permettre de répondre à une question comme : 
>Existe-t-il dans le grand Lyon un parc où je puisse jouer au basket pendant que ma petite sœur joue sur l'aire de jeux et que mon père promène son chien ?

**Sources des données :**
* la plateforme opendata de la métropole: https://data.grandlyon.com/

* Jeux de données sur les parcs et jardins de la métropole (`Parcs`): https://data.grandlyon.com/jeux-de-donnees/parcs-jardins-metropole-lyon/donnees
les données seront récupérées sur internet.

* Jeux de données sur les installations sportives (`Installations`):
https://data.grandlyon.com/jeux-de-donnees/recensement-equipements-sportifs-espaces-sites-pratique-metropole-lyon/info
les données sont fournies dans un fichier `Data_sport_2016.csv`
![Symbole Attention](/images/attention-sign.png "Attention !") Avant de continuer, pensez à récupérer les fichiers à l'endroit indiqué par votre professeur.
*Vous utiliserez DBbrowser pour vous connecter à la base de donnée et exécuter les requêtes SQL*

## I/ Une première tentative
On va utiliser les jeux de données de la métropole pour créer une première base de données.

![Symbole A faire](images/to-do-list.png "A faire")
1. créer un fichier `Base1.db`
2. dans DBbrowser, se connecter à `Base1.db`
3. dans DBrowser exécuter le script SQL `CommunesBadWKeys.sql` qui va créer la structure de la base de données.

**Question 1 :** 
En étudiant `CommunesBadWKeys.sql`, dressez la liste des tables contenues dans la base de données et des attributs de chaque table.

**Question 2 :**
À votre avis, quels sont les attributs qui viennent de `Parc` et quels sont ceux qui viennent de `Sport` ?

*Exécutez le script python `InsertIntoBadDbWKeys.py`, qui va récupérer les données et les insérer dans la base. Son fonctionnement n'est pas important pour l'instant.*

**Question 3 :**
Traduisez sous forme d'une requête SQL sur `base1.db` la question *Où puis-je faire du basket-ball à Lyon ?*

**Question 4 :**
Comment savoir si une installation sportive où je peux faire du basket-ball est située dans un parc ? Écrivez la requête SQL qui permettrait d'afficher les installations sportives de Lyon situées dans un parc. *On pourra considérer les installations/parcs qui ont soit la même adresse soit le même nom.* 

**Question 5 :**
Comment faire pour savoir si les chiens sont autorisés dans un parc ? Écrivez une requête SQL permettant de faire la liste des parcs où les chiens sont autorisés, puis une requête listant les parcs où les chiens ne sont pas autorisés.


**Exécutez la requête suivante:**
```SQL
INSERT INTO PARC ('Chien') VALUES ('Non');
```
**Question 6 :**
Que fait cette requête ?

**Question 7 :**
Pourquoi l'exécution de cette requête pose-t-elle problème ?


**Exécutez la requête suivante:**
```SQL
INSERT INTO PARC VALUES ('69382','Lyon','Square Widor','1','Place Charles Et Marie Widor','607.6', 'Non','Oui','Piéton','Aire De Jeux', 'Non','Non','Oui','Oui');
```
**Question 8 :**
Que se passe-t-il si vous exécutez maintenant les requêtes de la question 5 ? Pourquoi a-t-on ce problème ?

*On a affaire à une anomalie d'insertion : l'intégrité des données n'est pas respectée car on peut insérer plusieurs fois le même enregistrement, ce qui fait qu'on obtient des données parcellaires ou contradictoires.*

**Question 9 :**
Comment modifier la structure de la base de données pour que ce type d'anomalies ne soit plus possible sur les tables `Parc` et `Sport` ?

## II/ Une deuxième tentative

![Symbole A faire](images/to-do-list.png "A faire")
1. créer un fichier `Base2.db`
2. dans DBbrowser, se connecter à `Base2.db`
3. dans DBrowser exécuter le script SQL `CommunesBad.sql`, qui va créer la structure de la base de données.

*Exécutez le script python `InsertIntoBadDb.py`, qui va récupérer les données et les insérer dans la base. Son fonctionnement n'est pas important pour l'instant.*

**Question 10 :** 
Exécutez sur cette nouvelle base `Base2` les deux requêtes causant les anomalies d'insertion dans `Base1`. Que se passe-t-il ?

**Question 11 :** 
En étudiant `CommunesBad.sql`, dressez la liste des tables contenues dans la base de données et des attributs de chaque table.

**Question 12 :**
Quelles modifications ont été faites par rapport à la structure de la base de données précédente ? Expliquez en quoi ces modifications empêchent les anomalies observées dans `Base1`.

**Question 13 :**
Qu'effectue la requête suivante ?
```SQL
select NomParc,NomInstallation,p.Commune,s.Commune from PARC p join SPORT s on p.CodeInsee = s.CodeInsee where p.Voie = s.Voie and p.NoVoie = s.NoVoie;
```

**Question 14:**
L'INSEE effectue une mise à jour de ses codes communes. Le code INSEE de Bron passe de 69029 à 69999. Les services des sports de la métropole font la mise à jour sur la table `Parc` en exécutant la requête suivante :

```SQL
UPDATE PARC SET CodeInsee = 69999 WHERE CodeInsee = 69029;
```
Que se passe-t-il maintenant si on exécute à nouveau la requête de la question 13 ?

**Question 15:**
Nous venons de créer une anomalie de mise à jour. Expliquez pourquoi la mise à jour de la question 14 crée une anomalie.

**Question 16:**
De la même manière que nous avions modifié la structure de `Base1` pour éviter des anomalies d'insertion, quelle(s) modification(s) de la structure de `Base2` proposeriez-vous afin d'éviter l'anomalie de mise à jour que nous venons de créer à la question 14 ?

**Question 17:**
Y a-t-il d'autres attributs sur lesquels une modification du même type est nécessaire pour éviter d'autres anomalies de mise à jour ?

**Question 18:**
Saisir la requête suivante et observer le résultat :
```SQL
SELECT * FROM SPORT
WHERE NomInstallation = "Salle Maurice Jourdan"
```
Le conseil municipal de Champagne-au-Mont-d'Or pendant le temps des travaux l'activité Tennis de table de la base de données et on réalise la requête suivante :
```SQL
DELETE FROM SPORT
WHERE Commune = "Champagne-au-Mont-d'Or" and ActiviteCode = 8001
```
**Question 19 :**
Que constate-t-on si on cherche à nouveau les installations sportives de cette salle ? pourquoi ?
Quel problème cela pose-t-il ?
*L'anomalie observée est une "anomalie de suppression".*

## III/ Une troisième tentative

![Symbole A faire](images/to-do-list.png "A faire")
1. Repérer l'emplacement des fichiers `Base3.db` et `Communes.sql`
2. dans DBbrowser, se connecter à `Base3.db`


**Question 20 :**
À l'aide du script SQL `Communes.sql` représenter la structure de la base sous forme de schéma. Vous pourrez vous inspirer du schéma suivant : 
![Structure de la base](images/SQLMMystery.png "Structure de la base")
Issu de l'excellent [SQL Murder Mystery](https://mystery.knightlab.com/)

**Question 21 :**
Reprendre les requêtes des questions 7-8, 14 et 18-19.
Conclure sur l'intérêt d'une base normalisée. Quelles en sont les contraintes ?

**Question 22 : Et au fait ?**
Quelle requête peut-on écrire pour répondre à la question 
>Existe-t-il dans le grand Lyon un parc où je puisse jouer au basket pendant que ma petite sœur joue sur l'aire de jeux et que mon père promène son chien ?

## Base de données et script Python

*On va maintenant voir comment on peut utiliser un programme python pour recupérer automatiquement les données et créer une base de données utilisable.*

![Symbole A faire](images/to-do-list.png "A faire")
1. Ouvrir la base `Data_sport_2016.csv`, que nous avons utilisée pour les installations sportives, avec un tableur.
2. Aller à la page https://data.grandlyon.com/jeux-de-donnees/parcs-jardins-metropole-lyon/donnees d'où ont été extraites les données sur les parcs.
3. Ouvrir avec un éditeur le fichier `InsertIntoDB.py`.

**Question 23 :**
Dans `InsertIntoDB.py`, de quels types sont les variables `parcs`et `sport`? De quel(s) type(s) sont chaque élément de ces variables ?
*vous pouvez vous aider de la documentation des modules `csv` et `json`*

**Question 24 :**
Quelles différences peut-on constater entre les bases de données de départ et `Base3.db `? Toutes les données ont-elles été conservées ?
Comment ces changements ont-ils été obtenus ?

**Question 25 :**
Dans `InsertIntoDB.py`, les attributs ont été sélectionnés de 2 façons différentes pour les 2 bases de données utilisées. Expliquer ces deux méthodes.

**Question 26 :**
Dans `InsertIntoDB.py`, complétez la partie "insertion des données dans la base de données", écrivez le bloc d'instruction permettant d'insérer les données dans la table `activite`.

**Question 27 :**
Dans `InsertIntoDB.py`, compléter la fonction `executeRequeteSQL`. Cette fonction prend en argument une requête SQL et le nom d'une base de données, puis exécute la requête SQL sur la base de données.
Cette fonction doit pouvoir exécuter n'importe quelle requête présentée dans la première partie.

**Question 28 :**
Dans la fonction `executeRequeteSQL`, à quoi servent les instructions `try... except` ? Pourquoi les exceptions ont leurs noms précédés de `sq.` ?

## Conclusion:

On a vu trois types d'anomalies : anomalie de suppression, d'insertion et de mise à jour. 
L'un des principaux objectifs d'une base de données bien normalisée (bien structurée) est d'éviter l'apparition de ces anomalies. 
Rédiger trois paragraphes résumant, en cinq lignes maximum, ce qu'il faut retenir sur chaque anomalie. 


Sources/credits:
Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>